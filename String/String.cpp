#include <iostream>
#include <string>

using namespace std;

int main()
{
    string str;
    cout << "Enter Text: ";
    cin >> str;
    
    int select;
    int spaces = 0;
    int chars = 0;
    int nums = 0;
    do
    {
        system("clear");
        cout << "\tMenu\n";
        cout << "1 - Count Of Spaces\n";
        cout << "2 - Count Of Chars\n";
        cout << "3 - Count Of Digits\n";
        cout << "4 - Replace dots by commas And Output Reversed\n";
        cout << "5 - Search \"Hello\" words\n";
        cout << "0 - Exit\n\n";

        cout << "Text: " << str << endl;

        cout << "Choise: ";
        cin >> select;

        switch (select)
        {
        case 1:
            spaces = 0;
            system("clear");
            for (int i = 0;i < str.length();i++)
            {
                if (str[i] == ' ')
                {
                    spaces++;
                }
            }
            cout << "Spaces count: " << spaces << "\n";
            system("read");
            break;
        case 2:
            system("clear");
            chars = 0;
            for (int i = 0;i <= str.length();i++)
            {
                if (str[i] >= 'A' && str[i] <= 'Z'|| str[i] >= 'a' && str[i] <= 'z')
                {
                    chars++;
                }
            }
            cout << "Chars: " << chars << "\n";
            system("read");
            break;
        case 3:
            system("clear");
            for (int i = 0;i <= str.length();i++)
            {
                if (str[i] >= '0' && str[i] <= '9')
                {
                    nums++;
                }
            }
            cout << "Digits: " << nums << "\n";
            system("read");
            break;
        case 4:
            system("clear");
            while (str.find(",") < str.length())
            {
                str.replace(str.find(","), 1, ".");
            }
            for (int i = str.length()-1;i >= 0;i--)
            {
                cout << str[i];
            }
            cout << "\n";
            system("read");
            break;
        case 5:
            system("clear");
            int helloCount = 0;
            for(int i=0;i<str.size()-4;i++)
            {
                if(str.substr(i,5) == "Hello") ++helloCount;
            }
            cout << "Hello Count: " << helloCount << "\n";
            system("read");//PAUSE
            break;
        }
    } while (select != 0);

    return 0;
}
